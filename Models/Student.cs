using System;
using System.Collections.Generic;

namespace wildcard_field_demo.Models
{
    public class Student
    {
        public int StudentId { get; set; }
        public string StudentName { get; set; }
        public string Email {get; set;}
        public string Address {get; set;}
        public DateTime CreatedDate { get; set; }
        
        // this field is to simulate the scenario where 
        // POCO object fields are more than what has been defined in ES mappings
        // when we set "dynamic": "strict"        
        // public string SomeExtraField {get; set;}
        
        public List<Subject> Subjects {get; set;}
    }

    public class Subject
    {
        public int SubjectId {get; set; }
        public string SubjectName {get; set; }
    }
}
