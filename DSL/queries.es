GET students/_search
{
    "query": {
        "match_all": {}
    }
}

GET students/_search
{
    "query": {
        "wildcard": {
            "address.wildcard": {
                "value": "*kampung buloh*",
                "case_insensitive": true
            }
        }
    }
}

# wrap with should
GET students/_search
{
    "query": {
        "bool": {
            "should": [
                {
                    "wildcard": {
                        "address.wildcard": {
                            "value": "*kampung buloh*",
                            "case_insensitive": true
                        }
                    }
                }
            ]
        }
    }
}