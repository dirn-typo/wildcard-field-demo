# update existing index mapping
PUT students_test/_mapping
{
    "properties": {
        "billingAddress": {
            "type": "text",
            "fields": {
                "keyword": {
                    "type": "keyword",
                    "ignore_above": 512,
                    "normalizer": "lowercase"
                },
                "wildcard": {
                    "type": "wildcard"
                }
            }
        },
        "subjects": {
            "properties": {
                "subjectInfo": {
                    "type": "text",
                    "fields": {
                        "keyword": {
                            "type": "keyword",
                            "ignore_above": 256,
                            "normalizer": "lowercase"
                        },
                        "wildcard": {
                            "type": "wildcard"
                        }
                    }
                }
            }
        }
    }
}