POST _reindex
{
    "source": {
        "index": "students"
    },
    "dest": {
        "index": "students_test"
    },
    "script": {
        "lang": "painless",
        "source": "ctx._source.billingAddress = ctx._source.studentName + ' | ' + ctx._source.address; for(item in ctx._source.subjects) { if ((item.subjectId != null && item.subjectId != '') && (item.subjectName != null && item.subjectName != '')) { item.subjectInfo = item.subjectId + '@' + item.subjectName; } }"
    }
}

# this can be done in kibana dev tools
// POST _reindex
// {
//   "source": {
//     "index": "students"
//   },
//   "dest": {
//     "index": "students_test"
//   },
//   "script": {
//     "lang": "painless",
//     "source": """
//     ctx._source.billingAddress = ctx._source.studentName + ' | ' + ctx._source.address;
    
//     for(item in ctx._source.subjects) { 
//       if ((item.subjectId != null && item.subjectId != "") && 
//       (item.subjectName != null && item.subjectName != ""))
//       {
//         item.subjectInfo = item.subjectId + ' @ ' + item.subjectName;
//       }
//     }
//     """
//   }
// }