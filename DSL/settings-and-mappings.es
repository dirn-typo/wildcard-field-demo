PUT students
{
    "settings": {
        "number_of_shards": 1,
        "number_of_replicas": 0,
        "analysis": {
            "normalizer": {
                "lowercase": {
                    "type": "custom",
                    "filter": [
                        "lowercase"
                    ]
                }
            },
            "analyzer": {
                "email": {
                    "type": "custom",
                    "filter": [
                        "lowercase"
                    ],
                    "tokenizer": "uax_url_email"
                }
            }
        }
    },
    "mappings": {
        "dynamic": "strict",
        "properties": {
            "studentId": {
                "type": "long"
            },
            "studentName": {
                "type": "text",
                "fields": {
                    "keyword": {
                        "type": "keyword",
                        "ignore_above": 256,
                        "normalizer": "lowercase"
                    },
                    "wildcard": {
                        "type": "wildcard"
                    }
                }
            },
            "email": {
                "type": "text",
                "fields": {
                    "keyword": {
                        "type": "keyword",
                        "ignore_above": 256,
                        "normalizer": "lowercase"
                    },
                    "wildcard": {
                        "type": "wildcard"
                    }
                },
                "analyzer": "email"
            },
            "address": {
                "type": "text",
                "fields": {
                    "keyword": {
                        "type": "keyword",
                        "ignore_above": 256,
                        "normalizer": "lowercase"
                    },
                    "wildcard": {
                        "type": "wildcard"
                    }
                },
                "analyzer": "email"
            },
            "createdDate": {
                "type": "date",
                "format": "epoch_millis||date_optional_time"
            },
            "subjects": {
                "properties": {
                    "subjectId": {
                        "type": "long"
                    },
                    "subjectName": {
                        "type": "text",
                        "fields": {
                            "keyword": {
                                "type": "keyword",
                                "ignore_above": 256,
                                "normalizer": "lowercase"
                            },
                            "wildcard": {
                                "type": "wildcard"
                            }
                        }
                    }
                }
            }
        }
    }
}