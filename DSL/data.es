PUT students/_doc/1
{
    "studentId": 1,
    "studentName": "Ahmad Haidir Ahmad Najib",
    "email": "haidir@dummy.com",
    "address": "20-3-02, Mawar Apartment, TPP",
    "createdDate": "2021-10-17T00:00:00",
    "subjects": [
        {
            "subjectId": 1,
            "subjectName": "Physics"
        },
        {
            "subjectId": 2,
            "subjectName": "Mathematic"
        }
    ]
}

PUT students/_doc/2
{
    "studentId": 2,
    "studentName": "Nur Rohaidah Mohamad",
    "email": "rohaidah@gugel.com",
    "address": "Bandar Permaisuri, Kampung Buloh",
    "createdDate": "2021-10-17T00:00:00",
    "subjects": [
        {
            "subjectId": 3,
            "subjectName": "Malay Language"
        },
        {
            "subjectId": 2,
            "subjectName": "Mathematic"
        }
    ]
}

PUT students/_doc/3
{
    "studentId": 3,
    "studentName": "Nur Nabilah Ahmad Haidir",
    "email": "nabilah@yahoo.com",
    "address": "Taman Kenanga, Melaka",
    "createdDate": "2021-10-17T00:00:00",
    "subjects": [
        {
            "subjectId": 3,
            "subjectName": "Malay Language"
        },
        {
            "subjectId": 4,
            "subjectName": "English Professional Language"
        }
    ]
}