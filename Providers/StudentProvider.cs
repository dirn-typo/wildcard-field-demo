using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.Extensions.Configuration;

using Nest;

using wildcard_field_demo.Models;

namespace wildcard_field_demo.Provider
{
    public interface IStudentProvider
    {
        Task<bool> PutStudent(Student student);
        Task<IList<Student>> GetStudents(string search, int pageIndex, int pageSize);
        Task<IList<Student>> GetStudentsWithQueryDescriptor(string search, int pageIndex, int pageSize);
        Task<string> GetStudentsWithQueryDescriptorDebugInfo(string search, int pageIndex, int pageSize);
    }

    public class StudentProvider : IStudentProvider
    {
        private readonly ElasticClient _client;

        public StudentProvider(IConfiguration config)
        {
            var uri = config.GetValue<string>("ElasticUri");
            var userName = config.GetValue<string>("ElasticUserName");
            var password = config.GetValue<string>("ElasticPassword");
            var indexName = config.GetValue<string>("IndexName");

            var settings = new ConnectionSettings(new Uri(uri))
                .BasicAuthentication(userName, password)
                .DefaultIndex(indexName)
                .DefaultMappingFor<Student>(map => map.IdProperty(p => p.StudentId));  // this is field inference

            _client = new ElasticClient(settings);
        }

        public async Task<bool> PutStudent(Student student)
        {
            var response = await _client.IndexDocumentAsync<Student>(student);
            return response.IsValid;
        }

        public async Task<IList<Student>> GetStudents(string search, int pageIndex, int pageSize)
        {
            var param = WildCardSearchParam(search);

            # region basic wildcard query
            // var response = await _client.SearchAsync<Student>(s => s
            //     .Query(q => q
            //         .Wildcard(w => w
            //             .Field(f => f.Address.Suffix("wildcard"))
            //             .Value(param)
            //             .CaseInsensitive(true)
            //         )
            //     )
            //     .From(pageSize * pageIndex)
            //     .Size(pageSize)
            //     .Sort(s => s.Ascending(f => f.StudentId))
            // );
            # endregion

            var response = await _client.SearchAsync<Student>(s => s
                .Query(q => q
                    .Bool(b => b.Should(
                        s => s.Wildcard(w => w
                                .Field(f => f.StudentName.Suffix("wildcard"))
                                .Value(param)
                                .CaseInsensitive(true)
                            ),
                        s => s.Wildcard(w => w
                                .Field(f => f.Email.Suffix("wildcard"))
                                .Value(param)
                                .CaseInsensitive(true)
                            ),
                        s => s.Wildcard(w => w
                                .Field(f => f.Address.Suffix("wildcard"))
                                .Value(param)
                                .CaseInsensitive(true)
                            ),
                        s => s.Wildcard(w => w
                                .Field(f => f.Subjects.First<Subject>().SubjectName.Suffix("wildcard"))
                                .Value(param)
                                .CaseInsensitive(true)
                            )
                        )
                    )
                )
                .From(pageSize * pageIndex)
                .Size(pageSize)
                .Sort(s => s.Ascending(f => f.StudentId))
            );

            return response.Documents.ToList();
        }

        public async Task<IList<Student>> GetStudentsWithQueryDescriptor(string search, int pageIndex, int pageSize)
        {
            var param = WildCardSearchParam(search);
            var boolQueryDescriptor = new BoolQueryDescriptor<Student>();
            var wildcardContainer = new List<Func<QueryContainerDescriptor<Student>, QueryContainer>>();

            wildcardContainer.Add(q => q.Wildcard(w => w.Field(f => f.StudentName.Suffix("wildcard")).Value(param).CaseInsensitive(true)));
            wildcardContainer.Add(q => q.Wildcard(w => w.Field(f => f.Email.Suffix("wildcard")).Value(param).CaseInsensitive(true)));
            wildcardContainer.Add(q => q.Wildcard(w => w.Field(f => f.Address.Suffix("wildcard")).Value(param).CaseInsensitive(true)));
            wildcardContainer.Add(q => q.Wildcard(w => w.Field(f => f.Subjects.First<Subject>().SubjectName.Suffix("wildcard")).Value(param).CaseInsensitive(true)));
            boolQueryDescriptor.Should(wildcardContainer);

            var response = await _client.SearchAsync<Student>(s => s
                .Query(q => q
                    .Bool(b => boolQueryDescriptor
                    )
                )
                .From(pageSize * pageIndex)
                .Size(pageSize)
                .Sort(s => s.Ascending(f => f.StudentId))
            );

            return response.Documents.ToList();
        }

        public async Task<string> GetStudentsWithQueryDescriptorDebugInfo(string search, int pageIndex, int pageSize)
        {
            var param = WildCardSearchParam(search);
            var boolQueryDescriptor = new BoolQueryDescriptor<Student>();
            var wildcardContainer = new List<Func<QueryContainerDescriptor<Student>, QueryContainer>>();

            wildcardContainer.Add(q => q.Wildcard(w => w.Field(f => f.StudentName.Suffix("wildcard")).Value(param).CaseInsensitive(true)));
            wildcardContainer.Add(q => q.Wildcard(w => w.Field(f => f.Email.Suffix("wildcard")).Value(param).CaseInsensitive(true)));
            wildcardContainer.Add(q => q.Wildcard(w => w.Field(f => f.Address.Suffix("wildcard")).Value(param).CaseInsensitive(true)));
            wildcardContainer.Add(q => q.Wildcard(w => w.Field(f => f.Subjects.First<Subject>().SubjectName.Suffix("wildcard")).Value(param).CaseInsensitive(true)));
            boolQueryDescriptor.Should(wildcardContainer);

            var response = await _client.SearchAsync<Student>(s => s
                .RequestConfiguration(r => r
                    .DisableDirectStreaming()
                )
                .Query(q => q
                    .Bool(b => boolQueryDescriptor
                    )
                )
                .From(pageSize * pageIndex)
                .Size(pageSize)
                .Sort(s => s.Ascending(f => f.StudentId))
            );

            return response.DebugInformation;
        }

        private string WildCardSearchParam(string search)
        {
            var param = search.ToLowerInvariant();
            return $"*{param}*";
        }
    }
}