# Wildcard Field Demo

## Prerequisite

[Setup Elastic on your local](https://www.elastic.co/start)

## Project Setup

- Open Dev Tools in Kibana
- Copy DSL command from settings-and-mappings.es and paste it into Kibana Dev Tools
- Execute the command to create students index
- Next copy DSL command from data.es and paste it into Kibana Dev Tools
- Execute the command to add test documents into students index  
- Set Elastic URI, Username and Password at project launchSettings.json

## Objectives

- Wildcard search via wildcard field/s
- POCO not in-sync with index mappings