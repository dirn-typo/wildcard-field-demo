using System.Collections.Generic;
using System.Threading.Tasks;

using wildcard_field_demo.Models;
using wildcard_field_demo.Provider;

namespace wildcard_field_demo.Services
{
    public interface IStudentService
    {
        Task<bool> PutStudent(Student student);
        Task<IList<Student>> GetStudents(string search, int pageIndex, int pageSize);
        Task<IList<Student>> GetStudentsWithQueryDescriptor(string search, int pageIndex, int pageSize);
        Task<string> GetStudentsWithQueryDescriptorDebugInfo(string search, int pageIndex, int pageSize);
    }

    public class StudentService : IStudentService
    {
        private readonly IStudentProvider _studentProvider;

        public StudentService(IStudentProvider studentProvider)
        {
            _studentProvider = studentProvider;
        }

        public async Task<bool> PutStudent(Student student)
        {
            return await _studentProvider.PutStudent(student);
        }

        public async Task<IList<Student>> GetStudents(string search, int pageIndex, int pageSize)
        {
            return await _studentProvider.GetStudents(search, pageIndex, pageSize);
        }

        public async Task<IList<Student>> GetStudentsWithQueryDescriptor(string search, int pageIndex, int pageSize)
        {
            return await _studentProvider.GetStudentsWithQueryDescriptor(search, pageIndex, pageSize);
        }

        public async Task<string> GetStudentsWithQueryDescriptorDebugInfo(string search, int pageIndex, int pageSize)
        {
            return await _studentProvider.GetStudentsWithQueryDescriptorDebugInfo(search, pageIndex, pageSize);
        }
    }
}