using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

using wildcard_field_demo.Models;
using wildcard_field_demo.Services;

namespace wildcard_field_demo.Controllers
{
    [ApiController]
    public class StudentController : ControllerBase
    {
        private readonly IStudentService _studentService;

        public StudentController(IStudentService studentService)
        {
            _studentService = studentService;
        }

        [HttpPost]
        [Route("PutStudent")]
        public async Task<IActionResult> PutStudent(Student student)
        {
            var status = await _studentService.PutStudent(student);

            return Ok(status);
        }

        [HttpGet]
        [Route("GetStudents")]
        public async Task<IActionResult> GetStudents(string search, int pageIndex, int pageSize)
        {
            var students = await _studentService.GetStudents(search, pageIndex, pageSize);

            return Ok(students);
        }

        [HttpGet]
        [Route("GetStudentsWithQueryDescriptor")]
        public async Task<IActionResult> GetStudentsWithQueryDescriptor(string search, int pageIndex, int pageSize)
        {
            var students = await _studentService.GetStudentsWithQueryDescriptor(search, pageIndex, pageSize);

            return Ok(students);
        }

        [HttpGet]
        [Route("GetStudentsWithQueryDescriptorDebugInfo")]
        public async Task<IActionResult> GetStudentsWithQueryDescriptorDebugInfo(string search, int pageIndex, int pageSize)
        {
            var debugInfo = await _studentService.GetStudentsWithQueryDescriptorDebugInfo(search, pageIndex, pageSize);

            return Ok(debugInfo);
        }
    }
}